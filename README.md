# test-terraform-pre-hooks

<!-- BEGIN_TF_DOCS -->
## Requirements

No requirements.

## Providers

No providers.

## Modules

No modules.

## Resources

No resources.

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_I_am_variable"></a> [I\_am\_variable](#input\_I\_am\_variable) | value of variable | `string` | `"Hi, World!"` | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_name"></a> [name](#output\_name) | n/a |
| <a name="output_sensitive_name"></a> [sensitive\_name](#output\_sensitive\_name) | n/a |
<!-- END_TF_DOCS -->