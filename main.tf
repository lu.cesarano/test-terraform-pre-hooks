output "name" {
  value = "Hello, World!"
}

output "sensitive_name" {
  value     = "Hello, World!"
  sensitive = true
}

variable "I_am_variable" {
  description = "value of variable"
  default     = "Hi, World!"
}
