FROM alpine:3.17.0 as builder

ARG TFLINT_VERSION=0.44.1
ARG TFLINT_RULESET_AWS_VERSION=0.21.1

RUN wget --quiet -O /tmp/tflint.zip https://github.com/terraform-linters/tflint/releases/download/v"${TFLINT_VERSION}"/tflint_linux_amd64.zip \
  && unzip /tmp/tflint.zip -d /tmp

RUN wget -O /tmp/tflint-ruleset-aws.zip https://github.com/terraform-linters/tflint-ruleset-aws/releases/download/v"${TFLINT_RULESET_AWS_VERSION}"/tflint-ruleset-aws_linux_amd64.zip \
  && mkdir -p ~/.tflint.d/plugins/github.com/terraform-linters/tflint-ruleset-aws/"${TFLINT_RULESET_AWS_VERSION}" \
  && unzip /tmp/tflint-ruleset-aws.zip -d ~/.tflint.d/plugins/github.com/terraform-linters/tflint-ruleset-aws/"${TFLINT_RULESET_AWS_VERSION}" \
  && rm /tmp/tflint-ruleset-aws.zip

FROM ghcr.io/antonbabenko/pre-commit-terraform:v1.77.0

COPY --from=builder /tmp/tflint /tmp/tflint
# Overwrite with a newer version of tflint for better performance
RUN mv /tmp/tflint "$(which tflint)"

COPY --from=builder /root/.tflint.d /root/.tflint.d

COPY --from=golang:1.20.4-alpine3.17 /usr/local/go/ /usr/local/go/
ENV PATH="/usr/local/go/bin:${PATH}"
